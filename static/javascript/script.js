const socket = io();
let username = '';

$('#btnSetUsername').on('click', function () {
  document.getElementById('btnSend').disabled = false;
  document.getElementById('inputMessages').disabled = false;
  document.getElementById('btnSetUsername').disabled = true;
  username = $('#inputUsername').val();
  if(username.indexOf("<script>") !== -1)
  {
    socket.emit('message', "Are you trying to inject something ?");
  } else {
    socket.emit('message', `${username} joined the group`);
  }
  
});

$('form').submit((event) => {
  event.preventDefault();
  let text=$('#inputMessages').val()
  if(text.indexOf("<script>") !== -1)
  {
    text="Are you trying to inject something ?"
  }
  const msg = `${username}: ${text}`;
  socket.emit('message', msg);
  $('#inputMessages').val('');
});

socket.on('message', (msg) => {
  $('#chatBox').append(`<li>${msg}</li>`);
});
